__author__ = 'haishui'

import re
import string
import sys
def fixedName(string): #format valid names

  regex1= r'^[a-zA-Z]+$' #there is no - in name
  regex2=r'^[a-zA-Z]+\-{1,1}[a-zA-Z]+$' # there is one - in name
 
  if(re.match(regex1,string)) :
      string=string.lower()
      string=string[0].upper() + string[1:]
      return string
  elif(re.match(regex2,string)):
      print(string)
      location=string.find('-')
      string=string.lower()
      string=string[0].upper() + string[1:location]+string[location].upper()+string[location+1: ]
      return string
def identifyName(test): #identify name is valid or invalid
    regex = r'^[a-zA-Z]+\-{0,1}[a-zA-Z]+$' # you can also add a space in regex if u want to allow it in the string
    if re.match(regex,test):
       return 0
    else:
       return -1

def choiceMenu(ans):
   while ans:
    print("""
    Please to input Number for selection[0-3]:
    0.Open file<10000DirtyNames.csv>
    1.Open file<100000DirtyNames.csv>
    2.Open file<10000000DirtyNames.csv>
    3.Exit/Quit
    """)
    ans=input("What would you like to do? please input Menu Number[0-3]:")
    if ans=="0":
      print("\n 10000DirtyNames.csv will be proceed")
      return 0
    elif ans=="1":
      print("\n 100000DirtyNames.csv will be proceed")
      return 1
    elif ans=="2":
      print("\n 10000000DirtyNames.csv will be proceed")
      return 2
    elif ans=="3":
      print("\n Goodbye")
      ans = None
      return None
    else:
       print("\n Not Valid Choice Try again")

def readFile(flag,readFileName=[],originalList=[]):
   try:
      with open(readFileName[flag],'r') as readFileHandle:
          for line in readFileHandle:
             fields = line.split(',')
             for row in fields:
                 originalList.append(row)
      readFileHandle.close()            
   except IOError:
            print( "Error: File does not appear to exist.")
            sys.exit()

def  writeFile(fileName,myList=[]):
     try:
              writeClearnFile = open(fileName,'w')
             
              for x in myList:
                    writeClearnFile.write(str(x))
                    writeClearnFile.write(",")
              writeClearnFile.close()      
     except IOError:
              print( "Error: File can't be Created!.")
              sys.exit()

ans=True
flag=choiceMenu(ans)
if flag==None:
   print("See you next time")
   sys.exit()
else:
  readFileName=['10000DirtyNames.csv','100000DirtyNames.csv','10000000DirtyNames.csv']
  originalList=[]
  validList=[]
  invalidList=[]
 
  readFile(flag,readFileName,originalList)

  for i in range(0,len(originalList )-1):
         test=originalList[i]
         if (test==''):
             invalidList.append(test)
         else:
             id=identifyName(test)
             if (id==-1):
                 invalidList.append(test)
             else:
                 validList.append(fixedName(test))
  writeFile("CleanNames.csv",validList)
  writeFile("InvalidNames.csv",invalidList)
 
  
